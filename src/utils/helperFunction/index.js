export function latestSort(a, b) {
  return new Date(b.release_date).getTime() - new Date(a.release_date).getTime();
}

export function AverageRatingSort(a, b) {
  return parseFloat(b.average_rating) - parseFloat(a.average_rating);
}
