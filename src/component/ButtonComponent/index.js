import React from 'react';
import { StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { colors } from '../../utils';

function ButtonComponent({ title, onPress }) {
  return <Button buttonStyle={styles.button} title={title} onPress={onPress} />;
}

export default ButtonComponent;

const styles = StyleSheet.create({
  button: {
    backgroundColor: colors.background.secondary,
    borderRadius: 10,
    paddingVertical: 12,
  },
});
