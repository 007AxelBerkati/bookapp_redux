import NetInfo from '@react-native-community/netinfo';
import React, { useEffect } from 'react';
import { SafeAreaView } from 'react-native';
import { Provider, useSelector, useDispatch } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { NoInternet } from './component';
import { online, Persistore, Store } from './redux';
import Router from './router';

function AppWrapper() {
  const dispatch = useDispatch();
  const isOnline = useSelector((state) => state.dataBooks.isOnline);

  const removeNetInfo = () => {
    NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      dispatch(online(!offline));
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      {
      !isOnline ? <NoInternet /> : <Router />
      }
    </SafeAreaView>
  );
}

function App() {
  return (
    <Provider store={Store}>
      <PersistGate loading={null} persistor={Persistore}>
        <AppWrapper />
      </PersistGate>
    </Provider>
  );
}

export default App;
